Ciudad de México, 27 de marzo de 2017

**Comité Académico del** \
**Programa de Maestría y Doctorado en Filosofía,** \
**Universidad Nacional Autónoma de México**

Por este medio manifiesto mi disposición para dirigir la tesis, en calidad de tutor principal, del alumno **Ramiro Santa Ana Anguiano**, aspirante para ingresar a la Maestría en Filosofía con la investigación titulada *El creador y lo creado: la propiedad intelectual como supuesto en la creación cultural y filosófica* en el campo de conocimiento «Filosofía de la Cultura», previa autorización del Comité Académico.

_____________________________
Ernesto Priani Saisó
