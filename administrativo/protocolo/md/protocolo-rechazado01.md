# Protocolo [máx. 20,000 caracteres sin contar bibliografía; SON 33,000 CARACTERES]

## Título

Filosofía pirata: la propiedad intelectual como supuesto en la creación cultural y filosófica

## Planteamiento

Ante la tecnificación del quehacer cultural, en el que también se incluye el filosófico, la
filosofía pirata busca cuestionar sobre los elementos que parecen permanecer inmóviles ante
la expansión de la cultura impresa a lo digital. Dichos temas involucran las cuestiones
de la autoría, los nombres propios, la firma, la atribución, la publicación, la cita, la
acreditación, el uso justo y, en general, la creación y práctica intelectualººcite[]ºº dentro 
de centros de conocimiento así como en las propuestas periféricas o desescolarizadas.

Dentro de este movimiento y propuesta filosófica, cuyo principal canal de difusión es la
revista arbitrada y abierta *Culture Machine*, el término «piratería» no tiene un valor 
intrínseco, sea positivo o negativo.ººcite[]ºº La filosofía pirata no asiente que la
información debería de ser libre a toda costa, como está presente en varios colectivos
de piratería digital, que la compartición de información es un deseo «natural» humano, 
según argumenta diversas alas del Open Access (movimiento que busca el acceso abierto del
trabajo académico),ººcite[]ºº o que es un deber no cerrar el acceso a la información, 
perceptible en la formulación del imperativo categórico presente en el «Manifiesto GNU» de 
la Free Software Foundation (movimiento enfocado en la creación de *software* para que pueda 
ser usado, estudiado, distribuido y mejorado libremente).ººcite[]ºº

Por otro lado, la «piratería» no se trata como una consecuencia de la era digital y el
internet en detrimento del pilar económico que sostiene la mayoría de la producción cultural,
académica y filosófica. Tampoco enfoca su análisis al impacto de la piratería digital,
principalmente en y a través de los medios de comunicación. Es decir, el sentido y valor de 
la piratería permanece en cuestión.ººcite[]ºº

En su lugar, a través de los debates que ha abierto la piratería digital, el Open Access y el 
*software* libre o de código abierto, la filosofía pirata cuestiona diversos modelos que fueron 
erigidos en la era del papel como la autoría, la autoridad académica, la legitimación 
profesional, la propiedad intelectual y la revisión por pares.ººcite[]ºº El modo de preguntarse 
es principalmente ético fundado por el aparato crítico presente en la tradición filosófica, como
Derrida, Foucault, Barthes o Heidegger. El intento de respuesta pasa a través de cuestiones 
metodológicas hasta el impacto que esto tiene en el modo en como se crea y reproduce nuestra 
cultura. El principal enfoque dentro del quehacer cultural es en la práctica académica, 
principalmente en las humanidades y la filosofía, ya que son estas ramas las que más han basado 
su gestación y difusión a través del discurso en soporte impreso, en donde el texto no solo
es un medio de transmisión de información, sino también una expresión vinculada íntimamente con 
su locutor.ººcite[]ºº

Con estos elementos se puede indicar que la filosofía pirata es ética filosófica,ººcite[]ºº
pero también teoría metodológica y práctica cultural que pone a prueba los modelos culturales
predigitales y propone otros que desafían el binomio tecnocracia-capitalismo. Cabe decir que
en este movimiento el término «pirata» se aproxima a su acepción etimológica, del latín
«*pirata*» y del griego «*πειρατής*», que significa «poner a prueba», «intentar», «arriesgar»,
o «aventurar».ººcite[]ºº

Ahora bien, que la filosofía pirata parta del debate abierto por la piratería digital, el
Open Access y el *software* libre o de código abierto no implica que estos movimientos sean
uniformes en cuanto a sus objetivos y asunciones morales. Al contrario, en varias ocasiones
la relación entre estos se ha traducido en debate, roces e incluso confrontaciones y 
rompimientos. ¿Qué hace posible tratarlos como un conjunto?

La piratería digital es el movimiento menos unificado y uniforme, debido a que su actuar
en la mayoría de los casos es anárquico o de vertientes anarcoindividualistas por
lo que la existencia de aparatos institucionales queda limitada a organizaciones en
forma de colectivos o en agrupaciones entorno a plataformas de compartición de archivos 
como The Pirate Bay, LibGen o Aaaaarg. Se denomina «anárquica» ya que en diversas ocasiones la 
subida  de información carece de factores sociopolíticos y se orienta más al deseo de compartir 
sin la necesidad de intermediarios más que las computadoras.

Sin embargo, en algunas ocasiones surgen movilizaciones en defensa de la piratería digital, 
incluso hasta alcanzar un alto
grado de organización que expande sus exigencias más allá del marco técnico, como el 
movimiento civil que revirtió el acta de cese a la piratería (SOPA, por sus siglas en inglés)
en EE. UU.,ººcite[]ºº la casi meta alcanzada de The Pirate Bay de comprarse una isla en aguas 
internacionales para alojar sus servidores,ººcite[]ºº el surgimiento de los partidos 
pirata,ººcite[]ºº etcétera. El engrosamiento de las leyes de propiedad intelectual que
buscan restringir el acceso de la información son la principal causa del surgimiento de esta
conciencia grupal que, debido a su rechazo de las acciones cometidas por las instituciones
estatales o privadas, fundan su quehacer en un anarquismo individualista escéptico ante cualquier
forma de institución, sea conservadora o liberal. Las principales fuentes para su discurso
son clásicos del anarquismo, como Kropotkin, así como la «radicalización» de fundamentos morales
presentes en la ética *hacker*, como el principio de que la información *quiere ser* 
libre.ººcite[]ºº

El Open Access tiene su origen en la preocupación por la paulatina corporatización de 
instituciones universitarias, tradicionalmente consideradas como neutrales ante la presión 
del liberalismo económico. Por ello, este se enfoca en que la producción académica
esté disponible de manera gratuita para todo aquellos que cuenten con internet.ººcite[]ºº
Este movimiento tampoco es uniforme, por lo que pueden distinguirse dos caminos para poner
la información a disposición y dos posturas sobre el alcance que debería de tener la eliminación
de barreras.ººcite[]ºº No obstante, como movimiento existe el acuerdo de oponerse a las 
restricciones asociadas a los derechos de autor y las políticas de privacidad de editoriales 
académicas como Elsevier o JSTOR. Para ello se plantean una serie de lineamientos 
para la apertura de la información como son la creación de repositorios, archivos, editoriales
y *software* que ayuda a cubrir estas necesidades de la manera más transparente y democrática
posible.ººcite[]ºº

Si bien el Open Access se ha centrado principalmente en el quehacer académico de las ciencias,
paulatinamente ha surgido el interés de enfocarse también en las humanidades y la filosofía,
pero bajo otras formas debido a las características de sus discursos.ººcite[]ºº 
Esto también ha provocado que el carácter ético del movimiento tome mayor relevancia, ya que 
al parecer en las humanidades la obtención de regalías por la venta de publicaciones es un 
medio por el que varios investigadores pueden absorber los gastos que implica su labor.ººcite[]ºº 
Es decir, el principio es que *existe una ventaja* en abrir la información, pero la discusión 
es sobre cuál ha de ser el límite de esta apertura.

Desde un contexto histórico, el *software* libre o de código abierto antecede a los demás.
Este movimiento surgió ante la preocupación de la privatización del *software* en los
principios de la apertura digital de las universidades. Aunque se trate como un conjunto,
en realidad consiste en dos vertientes que en la práctica coinciden en que el código
fuente (el trabajo realizado por el programador) ha de estar disponible para cualquiera. 
Pero el trasfondo de esta necesidad es lo que ocasionó la temprana bifurcación de este 
movimiento, cuyo parteaguas fue el «Manifiesto GNU».

En este manifiesto se establece una ética deontológica, explícitamente vinculada a Kant,ººcite[]ºº
en el que la información *ha de ser y permanecer* libre como principio moral.
Varios programadores que consentían en el acceso del código fuente se perturbaron por la
intención moral o por las consecuencias económicas que puede implicar la
necesidad de que el código siempre esté disponible. Esto provocó la bifurcación del movimiento
en *software* libre y *software* de código abierto. El primero tiene un fuerte carácter
ético y social donde es deber del programador *liberar* su código en miras de que este
de alguna forma conlleve a dinámicas sociopolíticas o económicas distintas.ººcite[]ºº 
El segundo se rehúsa a darle connotaciones éticas a la *apertura* del código, por lo que 
se centran más en una perspectiva individualista y afín al liberalismo económico por el cual
se permite lucrar directamente con el código, por lo que la apertura permanente no es 
necesaria.ººcite[]ºº El punto de coincidencia es en que la información *debe de estar* disponible, 
pero divergen en si es una condición permeable al interés lucrativo mediante la privatización 
del código.

Con esto puede observarse que la piratería digital, el Open Access y el movimiento del
*software* libre o de código abierto comparten una postura hacia la información: que
esta *quiere* o *debe de* ser libre, y las ventajas que esto conlleva para el desarrollo
cultural dentro del contexto digital. Es en esta coincidencia que la filosofía pirata
se centra para partir su análisis. Sin embargo, como es perceptible, esto muchas veces
acarrea el problema de una falta de consenso sobre cómo habrá de tratarse a la información.

En este punto, es la propiedad intelectual la condición «negativa» de estos movimientos.
Si bien existe disparidades ante lo que positivamente ha de ser la eliminación
de barreras a la información, hay una «alianza» *de facto* en contra de las consecuencias
que puede acarrear la propiedad intelectual. Las iniciativas de limitar la piratería, la 
corporatización de las universidades y la privatización del código tienen su fundamento en la 
modificación y ampliación de los derechos de propiedad intelectual, cuyo impacto no solo es de 
índole técnica, jurídica o sociopolítica, sino también cultural, debido a que delimita los
modos de creación intelectual.

Esto ha llevado a que la justificación de la propiedad intelectual (PI) busque no solo basarse
en otras teorías jurídicas, como las concernientes a la propiedad física, libertad de expresión,
privacidad o protección ante la difamación,ººcite[]ºº sino también en fundamentos filosóficos 
que le den un mayor margen de legitimación. Por ello este ámbito parte de la distinción de 
tres dimensiones de la propiedad intelectual:

1. La PI como expresión concreta de una idea.
2. La PI como mecanismo de control, también llamada derecho de propiedad intelectual (DPI).
3. La PI como sistema que aglutina los diversos DPI, también conocido como sistema de 
propiedad intelectual (SPI).

La primera dimensión se enfoca principalmente en dos problemas: 1) ¿es posible diferenciar
entre la expresión y la idea?, 2) ¿toda creación intelectual puede tratarse como PI? La
primera pregunta es otra expresión del problema filosófico que implica el Mundo de las Ideas
de Platón, ya que se argumenta que semejante diferenciación solo es posible si se supone
que la idea preexiste a su concreción material o al menos en su expresión como 
pensamiento.ººcite[]ºº Dentro de la literatura sobre este tema no existe consenso sobre
esta cuestión, además de que diversos autores no la consideran esencial ya que el fundamento
de los DPI no depende directamente de esta respuesta.ººcite[]ºº

El segundo problema tiene un nexo directo con la segunda dimensión de la PI, ya que la
justificación de los DPI depende de cómo una creación intelectual puede tratarse
como propiedad. Al respecto, existen tres corrientes filosóficas:

1. La creación como mérito.
2. La creación como incentivo.
3. La creación como expresión.

La creación *como mérito* surge a partir de las ideas de Locke sobre la propiedad.ººcite[]ºº 
Esta indica un carácter normativo por el cual, al ser la creación fruto de un esfuerzo
mediante el trabajo, este ha de ser recompensado y regulado para evitar que otros se 
beneficien a expensas del creador.ººcite[]ºº Esto deriva en la constitución de derechos
económicos, los cuales son los fundamentos para la justificación jurídica de los DPI en el
mundo anglosajón.ººcite[]ºº

La creación *como incentivo* tiene sus orígenes en el utilitarismo.ººcite[]ºº Aquí
se busca que mediante el DPI los individuos se motiven a crear, labor que se traducirá
en progreso, beneficio o bienestar social.ººcite[]ºº Esta vertiente también desemboca en
la constitución de derechos económicos,ººcite[]ºº los cuales sirven de base para políticas
internacionales como el Convenio de Berna o el Acuerdo Transpacífico de Cooperación Económica.

La creación *como expresión* parte de las reflexiones de Hegel y Kant.ººcite[]ºº Para
esta corriente, la creación es la expresión de la voluntad o del discurso del sujeto
creador cuyo vínculo inalienable precisa de protección ante quienes puedan deformar
dicha expresión.ººcite[]ºº Esto funda una serie de derechos políticos para el sujeto,
los cuales se aglutinan en la doctrina de los derechos morales, común en las tradiciones
jurídicas «continentales» como la de Alemania, Francia o México.ººcite[]ºº

La disparidad entre corrientes en no muy pocas ocasiones acarrea la consecuencia
de legislaciones poco articuladas que precisan de muchos ajustes situacionales.ººcite[]ºº 
Esto implica que los DPI no solo deban basarse en las decisiones
tomadas en cortes, sino que deban de tener un fundamento filosófico por el cual sea 
posible erigir una teoría de la PI que sirva de guía para instituciones estatales e
internacionales.ººcite[]ºº

Esto lleva a la tercera dimensión, donde el SPI es la expresión jurídica de una teoría
general de la PI, la cual aún está lejos de concretarse.ººcite[]ºº A pesar de esta limitación 
y la falta de consenso entre corrientes, principalmente en lo que concierne a los 
límites en el control de la PI, estas comparten no solo el optimismo de que esta teoría 
es posible, sino que la PI y sus consecuentes derechos es una condición «positiva» para 
la adecuada reproducción de la cultura, debido a que los mecanismos de control evitan 
el colapso de diversas instituciones estatales, internacionales o privadas.

Aquí es donde se ve que existe una confrontación sobre el estatus ético, ontológico y cultural 
de la PI. Los movimientos de apertura de la información la tratan como una condición 
«negativa» (algo que *no ha de ser*); mientras que las corrientes filosófico-jurídicas 
la establecen como una condición «positiva» (algo que *ha de ser*) para la gestación e 
integridad de nuestra cultura. En este debate es donde la filosofía pirata no solo se 
rehusa a tomar partida para poder analizar las consecuencias, posibilidades y límites del 
debate abierto por esta confrontación, sino que también ayuda a dilucidar un fenómeno que 
es el problema en el que se enfoca esta investigación.

## Problema y tesis

Aunque históricamente los DPI tienen su origen a partir del siglo XVII y XVIII (Ley de Monopólios 
inglés y Estatuto de la Reina Ana),ººcite[]ºº el término de PI nació hasta el siglo XX,ººcite[]ºº
como eje rector para aglutinar una serie de doctrinas jurídicas como son los derechos de autor,
patentes, marcas, secretos comerciales, atributos de personalidad, denominaciones de 
origen, entre otros.ººcite[]ºº Esta conjunción surgió con la intención de empezar a articular y 
fundar desde un plano teórico-filosófico a estas juridicciones que se han visto afectadas por 
el advenimiento de la cultura digital y, con ello, de los críticos ante los modelos predigitales 
de crear, diseminar y mantener la información.

La corriente lockeana se basa en los escritos de Locke sobre la propiedad, sin que en ningún
momento este hablara de PI.ººcite[]ºº Con los utilitaristas la PI es una manifestación
más de un valor económico en pos de un bien social. Por último, en los personalistas,
Hegel y Kant reflexionaron sobre los derechos de autor.ººcite[]ºº En estas corrientes,
sus teóricos han buscado un traslado paulatino a la justificación de la PI,ººcite[]ºº 
no sin ambigüedades o paradojas debido a la dispar naturaleza de las diferentes 
manifestaciones en las que se da.ººcite[]ºº

A pesar de estas limitantes, cuya legitimación hermenéutica también es tema de análisis, estas
corrientes no solo se asimilan en la condición «positiva» dada a la PI, o en su origen como
respuesta ante el debate abierto por la crítica cultural surgida desde los modelos posibles
en la era digital, sino que todas comparten el supuesto donde la PI es un objeto en
relación con un sujeto creador.

Se habla de un «supuesto» ya que no existe una justificación expresa del porqué la PI se
trata a modo de un objeto, no solo desde las corrientes jurídico-filosóficas, sino también en
gran parte de los críticos de la PI, cuando el fenómeno entorno a su estatus ético, ontológico y 
cultural evidencia una compleja relación entre creador/creadores, instituciones estatales,
privadas, sin fines de lucro e internacionales, así como usuarios y público general donde
en varias ocasiones la PI no mienta un objeto y sus derechos de uso, sino *algo* que está
profundamente anclado en el entendimiento contemporáneo del mundo.ººcite[]ºº

Por ende, es posible preguntarse: *¿por qué la PI es tratada como un objeto vinculado a un 
sujeto creador, cuando el fenómeno de la creación cultural evidencia
que no es posible explicar ni justificar a la PI como el fruto de ese acto creativo e 
individualizado donde la participación más significativa de las instituciones y la sociedad es
antes, como incentivo o fuente de inspiración o de formación, o después, como protección
o fuente de consumo o de apreciación?*

Una propuesta de respuesta puede iniciar bajo la tentativa de que la PI se funda en un mito
donde el binomio creador-creación es de tal «naturaleza» que precisa de una protección
jurídica para salvaguardarla. La definición de la PI como «expresión concreta de una idea»ººcite[]ºº
no solo es la dimensión de la PI por la cual se desprenden el resto, sino que es la promovida
por la Organización Mundial de la Propiedad Intelectual e Indautor. Con esto se explicita 
que el carácter que se considera más íntimo es este acto creativo e individualizado de donde
*surge* la PI.

No obstante, esto no solo supone que la creación intelectual *puede* ser sinónimo de PI según 
la *posibilidad* de un valor económico, sino también que el acto creativo es esencialmente de 
índole individual, cuando de hecho este carece de fundamento, como justificación para la PI, 
si no se considera el correlato social que le otorga su significado y sentido. Es decir, la
relación objeto creado y sujeto creador de PI únicamente es significativa si este acto se
realiza a partir, sobre y para un contexto social: el mérito, el incentivo o la expresión 
vincula directamente a agentes externos a la relación sujeto-objeto, a tal grado que estos 
modos de entender a la creación requieren de un marco de legitimación social para su fundación.

El contexto social cede los DPIººcite[]ºº pero también es la posibilidad de PI matizada a través
de las instituciones que median entre le sujeto creador y el público, por lo que cabe hablar
de la PI como un mito, cuyo primer momento puede analizarse como un aparato ideológico. La
PI se entendería como un instrumento rector para la reproducción de conocimiento según los
modelos de la cultura predigital, mientras que sus críticos mostrarían puntos de relieve sobre
las distintas formas en como la PI permite la prolongación de un sistema político y económico
no solo estatal, sino también global. La PI se erigiría como uno de los mecanismos actuales
por los cuales se posibilita una especie de expansión ideológica interestatal cuya concreción 
está presente en diversos acuerdos comerciales donde la PI es ya el principal punto de referencia
para el control de la transferencia tecnológica y la economía que esto genera.

Es aquí donde los modelos de producción de conocimiento de la cultura digital, como el aprendizaje
autodidacta fomentado por las diversas comunidades digitales y la colaboración en línea,
se presentarían como un proceso de desescolarización principalmente orientado a una ética de
cuidadoººcite[]ºº de la información a partir de las personas que se encargan de ello. Esto 
también implicaría que estas prácticas son propuestas para limitar una ideología que, desde
estos movimientos, se percibe como nociva para el quehacer cultural. Por ende, la condición
«negativa» de la PI se traduciría en varias formas de organización para detener la reproducción
de una ideología dominante.

Pero este análisis sería insuficiente si no se explora el origen y fundamento de semejante mito
fijado como aparato ideológico. La PI como «expresión concreta de una idea» denota una particular
relación sujeto-objeto, donde su personalización delimita que no se trata de cualquier vínculo
sino uno donde la creación es ininteligible sin su creador y viceversa. La palabra clave podría 
ser «expresión», ya que esto evidencia que de las corrientes que justifican la PI, es la
hegeliana-kantiana la que destaca del resto debido a que en esta el tema de la PI no es
derivado a una economía monetaria que, por sí misma, podría diluir los DPI en otras doctrinas
jurídicas que la anteceden.

La corriente personalista establece un vínculo inalienable donde la creación no puede
separarse de su creador. Sin embargo, esto también demuestra que el creador no antecede a la
creación, debido a que sin objeto creado, el sujeto es incomprensible como *sujeto creador*.
Es decir, si la principal característica de la PI es la creación como expresión, entonces 
esta expresividad es en un doble sentido: manifiesta un objeto creado al mismo tiempo que 
funda un sujeto creador.

Por lo tanto, si bien en la justificación de la PI como expresión indica que el sujeto creador
antecede a la creación, este confunde las entidades (el sujeto y el objeto) con el valor e
intención discursiva de estas (el sujeto *como creador* y el objeto *como creación*). Esta
confusión cabría interpretarse como un fruto de la modernidad,ººcite[]ºº el cual supone que
una entidad «neutral» delimita su propio significado y sentido (el *sujeto creador*
precedente o incluso sin tener presente un *objeto creado*). Ante semejante proceso de
individuación,ººcite[]ºº podría analizarse al sujeto creador como una figura legitimada
socialmente y protegida institucionalmente que sirve de base para comprender un objeto
como creado *por el* sujeto al unísono que adquiere el estatus de creador, una 
especie de funciónººcite[]ºº que en la jerga del desarrollo de *software* se define como 
función recursiva: una que se llama a sí misma.

En este sentido, la condición «negativa» de la PI podría interpretarse como la preocupación
de que tal función tenga una falla sistemática que provoque una iteración infinita. Es decir, 
que existe la posibilidad de que en la repetición de la función-creador paulatinamente paralice
otros aspectos del quehacer cultural, como la diseminación y mantenimiento de la información.
De ser así, existiría una explicación del porqué históricamente en un principio la cuestión de 
la autoría y su consecuente protección jurídica no representó un problema, y cómo actualmente
su iteración de manera paulatina implica el aislamiento de la creación cultural a canales
de acceso cada vez se ven más constreñidos a una determinada lógica económica.

De esta manera es como la PI se trataría no como un objeto, sino como un supuesto cuya
condición de posibilidad se discierne a través del fenómeno dilucidado en la confrontación
sobre el estatus ético, ontológico y cultural de la PI, para lo cual se requiere el
cumplimiento de los siguientes objetivos.

## Objetivos

### Generales

1. Analizar el carácter común de la piratería digital, el Open Access y el *software* libre
o de código abierto que perfilan a la PI como una condición «negativa» para la cultura
debido a ciertos nuevos modelos económicos, culturales y políticos frutos del advenimiento 
digital.

2. Analizar el carácter común de las corrientes lockeana, utilitarista y helegiana-kantiana
que perfilan a la PI como una condición «positiva» para la cultura anclada en modelos
económicos, culturales y políticos originados en la era predigital.

3. Poner a prueba las posibilidades que pueden desatarse a partir de la confrontación entre
ambas condiciones para tratar a la PI como un supuesto en lugar de un objeto privilegiado
en el quehacer cultural contemporáneo.

### Particulares

1. Dilucidar cuáles son las diferencias de la piratería digital, el Open Access y el 
*software* libre o de código abierto respecto al modo de tratar la información.

2. Delinear las dos características que comparten la piratería digital, el Open Access y
el *software* libre o de código abierto sobre la información y la PI a partir de los
nuevos modelos de creación cultural posibles en la era digital.

3. Esclarecer las especificidad de las corrientes lockeana, utilitarista y 
hegeliana-kantiana que permite diferenciar las distintas dimensiones de la PI.

4. Delimitar las dos características que comparten las corrientes lockeana, utilitarista
y hegeliana-kantiana en torno a la información y la PI a partir de los modelos
de creación cultural establecidos antes del advenimiento de las computadoras y el internet.

5. Explorar las posibilidades de tratar a la PI no como un objeto, sino como un mito
cuya fijación se expresa como un aparato ideológico interestatal que fomenta y cierne
a la información a un modelo económico-político de carácter hegemónico, así como las
alternativas que filtran y muestran el carácter mítico de la PI.

6. Rastrear el origen del mito de la PI como un fruto de la modernidad, cuyo eje rector es 
la función-creador, la cual funda y reproduce la idea de la PI como un objeto cultural
privilegiado en lugar de evidenciarlo como un supuesto para el quehacer cultural 
contemporáneo en pos de las posibilidades y límites de la digitalización de la cultura.

## Relevancia

### Relevancia filosófica

1. La filosofía pirata, que emplea a la tradición filosófica como eje de análisis de
diversos temas de índole económica, política, jurídica y social, da una propuesta de
cómo es posible el quehacer filosófico interdisciplinario en el contexto abierto por
las tecnologías de la información y la comunicación digitales.

2. Los debates abiertos por la piratería digital, el Open Access y el *software* libre
o de código abierto permiten analizar una serie de fenómenos de relevancia filosófica,
ya que sus análisis son posibles desde diversos campos como la filosofía de la cultura, 
ética y epistemología, lo cual permite cuestionarnos sobre las posibilidades y límites
de nuestra herencia cultural y filosófica.

3. La búsqueda de fundamentación filosófica de la PI en las corrientes lockeana,
utilitarista y hegeliana-kantiana permite entrever cómo la disparidad de tradiciones
filosóficas pueden aglutinarse para fundar un mecanismo de control de la actividad
intelectual que en sí mismo es analizable como fenómeno que impacta y reproduce la
práctica filosófica contemporánea.

### Relevancia no filosófica

1. El deseo de crear alternativas a la PI ha generado una serie de dinámicas
culturales que desafían los modelos tradicionales de quehacer cultural a través
de las posibilidades permitidas por la era digital, para lo cual en su base subyacen
una serie de supuesto éticos, filosóficos y metodológicos sobre lo que es la 
creación intelectual.

2. La búsqueda de justificar a la PI tiene un impacto directo sobre el modo en como
las relaciones comerciales internacionales se desenvuelven, ya que es la PI el
fundamento por el cual se generan acuerdos para el control de transferencia
tecnológica y la generación de economías acordes al panorama político y
económico actual.

3. Para el caso de América Latina, la economía genera por la PI es «ampliamente 
deficitaria»,ººcite[]ºº valuada en un una balanza negativa anual de miles de
millones de dólares,ººcite[]ºº afectando así la creación de infraestructura 
cultural y tecnológica en los países de la región y cuya recomendación internacional
es fomentar, centralizar y fortalecer una economía basada en los DPI,ººcite[]ºº 
ante un contexto internacional de amplia desventaja ante los países desarrollados,
India o China.

### Aportación original

1. La filosofía pirata es ética filosófica en los orígenes de su constitución;
en esta investigación se utilizará como base metodológica para el análisis
filosófico de la PI a partir del cuestionamiento sobre el valor de la
«piratería» para avanzar a temáticas de filosofía de la cultura y epistemología.

2. Las corrientes filosóficas que buscan justificar la PI centran su análisis
en sus diferencias y similitudes en pos de una teoría general de la PI o,
por lo menos, a favor de cierta clase de regulación jurídica de la PI; en esta
investigación estas corrientes filosóficas se cuestionan como diversas
manifestaciones que pretenden justificar a la PI a partir del mantenimiento
de modelos culturales surgidos en la era predigital que hasta cierto punto
también implica una justificación de los aparatos económicos, políticos y
culturales que delimitan el modo de creación intelectual en la actualidad.

3. En el estado actual de la confrontación sobre el estatuto de la PI, esta es
tratada principalmente como un objeto; en esta investigación se buscará expandir 
su esfera a un fenómeno que evidencia a la PI como un supuesto basado en un mito,
fijado en forma de un aparato ideológico interestatal, cuyo principal sostén
es la función-creador.

## Índice tentativo

Introducción. La filosofía pirata como método filosófico.

1. La PI como condición «negativa» para el quehacer cultural.
  1. Del impacto cultural al preguntar filosófico abierto por la «piratería».
  2. Los tres movimientos para la redefinición del carácter de la información.
    1. La piratería digital o la información liberada sin más.
    2. El Open Access o la ventaja de la apertura de la información.
    3. El *software* libre o de código abierto, o los límites de la liberación de la información.
  3. La PI como un problema para los nuevos modelos de la cultura digital.
2. La PI como condición «positiva» para el quehacer cultural.
  1. Las tres dimensiones de la PI.
  2. Las tres corrientes filosóficas para justificar la PI.
    1. La justificación lockeana o la creación como mérito.
    2. La justificación utilitarista o la creación como incentivo.
    3. La justificación hegeliana-kantiana o la creación como expresión.
  3. La fundación de la PI como objeto y paradigma de la cultura predigital.
3. La PI como un supuesto para el quehacer cultural.
  1. La PI como mito dentro de la creación intelectual.
  2. La PI como aparato ideológico interestatal.
  3. La función-creador como origen, fundamento y prolongación de la PI.

Conclusión. La filosofía como liberación anticipada y continua del conocimiento.

## Cronograma

El cronograma está basado en un calendario de 24 meses. La investigación desde su principio
ya está disponible en [xxx.cliteratu.re](http://xxx.cliteratu.re), el cual es un repositorio
que contiene toda la información relativa a este trabajo. Además, se creará un *script* escrito
en Ruby que analizará las conexiones entre la bibliografía para encontrar relaciones; los nodos
representarán las referencias bibliográficas, cuyo tamaño será relativo a su cantidad de
menciones entre la misma red de bibliografía de la investigación; los vectores serán las
relaciones entre cada una de estas entradas bibliográficas. De esta manera servirá para
encontrar bibliografía relevante para esta investigación entre otras cuestiones aún por
conocer.

|  Mes  |           Actividad principal           |          Actividad secundaria           |
|-------|-----------------------------------------|-----------------------------------------|
|   1   |  Mejoramiento del acceso al repositorio |      Ajustes finales al protocolo       |
|   2   |         Desarrollo del *script*         |    Ajustes al acceso del repositorio    |
|   3   |      Puesta en marcha del *script*      |  Corrección de errores en el *script*   |
|  4-6  |  Análisis de los datos bibliográficos   |   Incorporación de nueva bibliografía   |
| 7-10  |     Desarrollo de la primera parte      |        Ajustes a la bibliografía        |
| 11-14 |     Desarrollo de la segunda parte      |     Correcciones de la primera parte    |
| 15-18 |     Desarrollo de la tercera parte      |     Correcciones de la segunda parte    |
| 19-21 |  Desarrollo de la conclusión e intro.   |     Correcciones de la tercera parte    |
| 22-23 |      Correcciones y ajustes finales     |    Mantenimiento a los archivos *web*   |
|  24   |      Creación del PDF, EPUB y MOBI      |      Mantenimiento del repositorio      |

## Bibliografía

### Primaria

ALTHUSSER, Louis. *Ideología y aparatos ideológicos del Estado*. 1970. 

BARLOW, John Perry. «Vender vino sin botellas». *¿Propiedad intelectual? Una recopilación de ensayos críticos*. Perro Triste. 2016.

BARRON, Anne. «Kant, Copyright and Communicative Freedom». *Law and Philosophy*. 2012, vol 31, núm. 1, p. 1-48.

BARTHES, Rol. *La muerte del autor*. 1968.

CHILD, James W. «The Moral Foundations of Intangible Property». *The Monist*. 1990, vol 73, núm. 4, p. 578-600.

DERRIDA, Jacques. «The future of the profession or the university without condition (thanks to the “Humanities,” what could take place tomorrow)». *Jacques Derrida and the Humanities*. Cambridge University Press. 2002. p. 24-57. 

DRAHOS, Peter. *A Philosophy of Intellectual Property (Applied Legal Philosophy)*. Dartmouth Pub Co, 1996. 

FISHER, William. «Teorías de la propiedad intelectual». *¿Propiedad intelectual? Una recopilación de ensayos críticos*. Perro Triste. 2016.

FOUCAULT, Michel. *¿Qué es un autor?*. 1999.

HALL, Gary. «Introduction: Pirate Philosophy». *Culture Machine*. 2009, vol 10, p. 1-5.

HALL, Gary. «Pirate Philosophy (Version 1.0): Open Access, Open Editing, Free Content, Free/Libre/Open Media». *Culture Machine*. 2009, vol 10, 

HALL, Gary. «Pirate Radical Philosophy». *Radical Philosophy*. 2012, núm. 173, p. 33-40.

HALL, Gary. *Pirate Philosophy: For a Digital Posthumanities*. MIT Press, 2016. 

HALL, Gary y ADEMA, J. «Posthumanities: The Dark Side of »The Dark Side of the Digital«». *Journal of Electronic Publishing*. 2016, vol 19, núm. 2, 

HEGEL, Georg Wilhelm Friedrich. «La propiedad». *Principios de la filosofía del derecho*. Editora y Distribuidora Hispano Americana, S.A. (EDHASA). 2005. p. 125-160. 

HEIDEGGER, Martin. «La pregunta por la técnica». *Conferencias y artículos*. Ediciones del Serbal. 1994. p. 9-37. 

HUGHES, Justin. «The Philosophy of Intellectual Property». *Georgetown Law Journal*. 1988, 

ILLICH, Iván. *La sociedad desescolarizada*. Joaquín Mortiz — Planeta, 1985. 

KANT, Immanuel. *La metafísica de las costumbres*. Editorial Tecnos, 2005. 

KUFLIK, Arthur. «The Moral Foundations of Intellectual Property Rights». *Owning Scientific and Technical Information*. Rutgers University Press. 1989.

LESSIG, Lawrence. *Cultura libre*. LOM Ediciones, 2011.

LEVER, Annabelle. *New Frontiers in the Philosophy of Intellectual Property*. Cambridge University Press, 2012.

LOCKE, John. «De la propiedad». *Segundo Tratado sobre el Gobierno Civil*. Tecnos. 2006. p. 32-55. 

MOORE, Adam y HIMMA, Ken. «Intellectual Property». *The Stanford Encyclopedia of Philosophy*. Winter 2014 ed. 2014, 

MOORE, Adam D. «Personality-Based, Rule-Utilitarian, and Lockean Justifications of Intellectual Property». *The Handbook of Information and Computer Ethics*. John Wiley & Sons, Inc. 2008. p. 105-130. 

MOORE, Adam D. «A Lockean Theory of Intellectual Property Revisited». *San Diego Law Review*. 2012, vol 50, 

RAYMOND, Eric Steven. «La catedral y el bazar». *¿Propiedad intelectual? Una recopilación de ensayos críticos*. Perro Triste. 2016.

ROJO, Facundo. «Fundamentos filosóficos de la doctrina del fair use». *¿Propiedad intelectual? Una recopilación de ensayos críticos*. Perro Triste. 2016.

SCHROEDER, Jeanne. *Unnatural Rights: Hegel And Intellectual Propery*. 2004.

SHIFFRIN, Seana Valentine. «Intellectual Property». *A Companion to Contemporary Political Philosophy*. Blackwell. 2007. p. 653-668. 

STALLMAN, Richard. «El manifiesto de GNU». *¿Propiedad intelectual? Una recopilación de ensayos críticos*. Perro Triste. 2016.

STENGEL, Daniel. «Intellectual Property in Philosophy». *Archives for Philosophy of Law and Social Philosophy*. 2004, vol 90, núm. 1, p. 20-50.

VV. AA. Budapest Open Access Initiative, XV aniversario. 2016.

### Secundaria

ARKAUTE, Natxo Rodríguez. «Producción artística y copyleft en el nuevo entorno digital». *Propiedad intelectual, nuevas tecnologías y libre acceso a la cultura*. Centro Cultural de España en México — Universidad de las Américas Puebla. 2008.

ARTEAGA, Carmen. «Marco legal del derecho de autor en México». *Propiedad intelectual, nuevas tecnologías y libre acceso a la cultura*. Centro Cultural de España en México — Universidad de las Américas Puebla. 2008.

BOETA, Sergio Augusto. «¿Limitar legalmente el acceso a la cultura?». *Propiedad intelectual, nuevas tecnologías y libre acceso a la cultura*. Centro Cultural de España en México — Universidad de las Américas Puebla. 2008.

BOLLIER, David. «El redescubrimiento del procomún». *¿Propiedad intelectual? Una recopilación de ensayos críticos*. Perro Triste. 2016.

BRAVO BUENO, David. *Copia este libro*. 2005. 

BREAKEY, Hugh. «Natural Intellectual Property Rights and the Public Domain». *The Modern Law Review*. 2010, vol 73, núm. 2, p. 208-239.

DERRY, T.K. y WILLIAMS, T.I. *Historia de la tecnología*. Vol. 1. Siglo XXI, 1990. 

DERRY, T.K. y WILLIAMS, T.I. *Historia de la tecnología*. Vol. 2. Siglo XXI, 1991. 

DERRY, T.K. y WILLIAMS, T.I. *Historia de la tecnología*. Vol. 3. Siglo XXI, 1991. 

ECO, Umberto. *Obra abierta*. Epublibre, 2016. 

GILMORE, John. «Privacidad, tecnologia y sociedad abierta». *:() :|:& ;: Internet, hackers y software libre*. Editora Fantasma. 2004.

HETTINGER, Edwin C. «Justifying Intellectual Property». *Philosophy & Public Affairs*. 1989, vol 18, núm. 1, p. 31-52.

HIMMA, Kenneth Einar. «Justifying Intellectual Property Protection: Why the Interests of Content-Creators Usually Wins Over Everyone Else’s». *Information, Technology and Social Justice*. Idea Group. 2006.

JIMÉNEZ, Lucina. «Escuela, arte y nuevas tecnologías». *Propiedad intelectual, nuevas tecnologías y libre acceso a la cultura*. Centro Cultural de España en México — Universidad de las Américas Puebla. 2008.

LENK, Christian y ANDORNO, Roberto. *Ethics and Law of Intellectual Property*. Ashgate, 2007.

LESSIG, Lawrence. *The Future of Ideas: The Fate of the Commons in a Connected World*. Random House, 2001. 

LESSIG, Lawrence. *El código 2.0*. Traficantes de Sueños, 2009. 

LESSIG, Lawrence. *Remix : cultura de la remezcla y derechos de autor en el entorno digital*. Icaria editorial, 2012. 

LÉVY, Pierre. «El anillo de oro». *¿Propiedad intelectual? Una recopilación de ensayos críticos*. Perro Triste. 2016.

LÓPEZ, Alberto y RAMÍREZ, Eduardo. «Los derechos de autor en la era del capitalismo cultural». *Propiedad intelectual, nuevas tecnologías y libre acceso a la cultura*. Centro Cultural de España en México — Universidad de las Américas Puebla. 2008.

MCLEOD, Kembrew. «Crashing the Spectacle: A Forgotten History of Digital Sampling, Infringement, Copyright Liberation and the End of Recorded Music». *Culture Machine*. 2009, vol 10, p. 114-130.

MING, Wu. «Copyright y maremoto». *¿Propiedad intelectual? Una recopilación de ensayos críticos*. Perro Triste. 2016.

NIVÓN, Eduardo. «Propiedad intelectual y política cultural». *¿Propiedad intelectual? Una recopilación de ensayos críticos*. Perro Triste. 2016.

ORTMANN, Cecilia. «Software libre, géneros y (des)igualdad: expandir los horizontes de la libertad». *¿Propiedad intelectual? Una recopilación de ensayos críticos*. Perro Triste. 2016.

PALMER, Tom G. «Are Patents and Copyrights Morally Justified? The Philosophy of Property Rights and Ideal Objects». *Harvard Journal of Law & Public Policy*. 1990, vol 13, núm. 3, p. 817-865.

PAPATHÉODOROU, Aris. «Propiedad intelectual, copyright, patentes». *¿Propiedad intelectual? Una recopilación de ensayos críticos*. Perro Triste. 2016.

RANGEL MEDINA, David. «I. Conceptos fundamentales del derecho intelectual mexicano». *Panorama del derecho mexicano. Derecho intelectual*. UNAM. 2016.

RENDUELES, César. «Copiar, robar, mandar». *¿Propiedad intelectual? Una recopilación de ensayos críticos*. Perro Triste. 2016.

RINGENBACH, Jorge. «El proyecto Creative Commons en México». *¿Propiedad intelectual? Una recopilación de ensayos críticos*. Perro Triste. 2016.

ROSE, Mark. *Authors and Owners: The Invention of Copyright*. Harvard University Press, 1993. 

SÁNCHEZ, León Felipe. «¿Software libre o de élite?». *¿Propiedad intelectual? Una recopilación de ensayos críticos*. Perro Triste. 2016.

STALLMAN, Richard. «¿Por qué las escuelas deberian usar solo software libre?». *:() :|:& ;: Internet, hackers y software libre*. Editora Fantasma. 2004.

WAYNER, Peter. *Free for All: How Linux and the Free Software Movement Undercut the High-Tech Titans*. HarperBusiness, 2000. 

WILLIAMS, Sam. *Free as in Freedom (2.0)*. Free Software Foundation, Inc, 2010. 

WILLIAMS, T.I. *Historia de la tecnología*. Vol. 4. Siglo XXI, 1988. 

WILLIAMS, T.I. *Historia de la tecnología*. Vol. 5. Siglo XXI, 1988. 

YÚDICE, George. «El copyright: instrumento de expropiación y resistencia». *Propiedad intelectual, nuevas tecnologías y libre acceso a la cultura*. Centro Cultural de España en México — Universidad de las Américas Puebla. 2008.

VV. AA. *América Latina: la balanza comercial en propiedad intelectual*. 2015. 

VV. AA. *Ley Federal del Derecho de Autor*. 1996.
