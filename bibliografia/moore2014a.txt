---------------------------------------- Extensión de 100 c.----------------------------------------

PI = Propiedad no física producto del pensamiento original.
  - Los derechos no se refieren a entidades no-físicas.

  => PI se dirige al control de expresiones concretas.
    * Protección de los intereses del creador.

=> Historia que va desde más allá de la antigua Grecia:
  - Los sistemas legales actuales y sus justicaciones morales se orientan a:
    * Personalismo.
    * Utilitaristas.
    * Lockeanas.

# Historia de la PI

En la antigüedad:
  - 500 a. c. una colonia griega se le concede el monopolio para la creación de ciertos
  artículos culinarios.
  - 257-180 a. c. Vitruvius revela que un poeta robó el trabajo de otro.
  - S I, distintos juristas romanos discutieron los intereses de autoría del trabajo intelectual.
  - En Roma, Fidentinus descubre que Martial utilizó el trabajo de otros sin citarlos.

  => Casos atípicos
    - En Grecia y Roma al parecer nunca tuvieron sistemas para los DPI.

De Roma a la república florentina:
  - Se crean franquicias y se conceden favores reales que involucran PI.
    * No son sistemasde PI porque restringen el acceso al dominio público:
    le quitan algo a la gente.
  - En 1421 se concede la primera patente a Brunelleschi.

Durante la república veneciana:
  - En 1474 se crea la primera institución para patentes.

Sistema inglés:
  - En 1624 se crea la Ley de Monopolios que concede 14 años de exclusividad y no concede derechos
  a lo que ya pertenece al dominio público.
  - En 1710 se crean los Estatutos de Ana, considerada la primera Ley del Derecho de Autor, que da
  14 años de exclusividad con una renovación de otros 14 años si el autor seguía vivo.

A partir de entonces empezó a extenderse a otros países europeos,
hasta la proyección internacional como en:
  - El Convenio de Berna.
  - El Acuerdo ADPIC.

# El dominio de la propiedad intelectual

## Derechos de autor (copyright)

Son trabajos originales sobre cualquier medio tangible.
  - No tiene que tener una función útil.
  - Expresiones concretas.
  - Garantía a cinco derechos:
    1. Reproducción.
    2. Adaptar/derivar.
    3. Distrubición.
    4. Hacerlo público.
    5. Interpretarlo públicamente.

    => En EE. UU. el creador puede ceder o vender algunos de estos derechos.

  - Dos restricciones:
    1. Uso justo (*fair use*): uso limitado de otros para otros propósitos.
    2. Primera venta: impedimento a interferir en las subsecuentes ventas.

## Creative Commons, copyleft y licencias

Nueva aproximación al copyright desde la regla de la primera venta, donde
  1. con el uso de licencias se permite un uso en específico,
  2. con un modelo de copyleft se busca expandir el uso.

## Patentes

Descubrimientos o inventos útiles.
  - Tres tipos de patentes:
    1. Patentes útiles para nuevos inventos o descubrimientos.
    2. Diseño de patentes para nuevos diseños.
    3. Patentes de plantas: para nuevas variedades de plantas.

    => La forma más fuerte de PI al conceder el monopolio por tiempo limitado.

  - Restricciones:
    1. Deben de ser útiles.
    2. Deben de ser innovadoras.
    3. Deben de no ser obvias.

## Secretos comerciales

Abarca una gran cantidad de contenidos sujetos a medidas privadas para mantener
la exclusividad de una potencial ventaja económica.
  - Restricciones:
    1. Debe de ser confidencial.
    2. Debe de dar una ventaja competitiva.
    3. No protege si el secreto es descubierto.
      * Pero existen medidas que protegen la apropiación inadecuada por
      espionaje o robo de empleados.

## Marcas

Elemento que identifica un bien o servicio y los distinguen de otros.
  - Restricciones:
    1. No debe de ser de uso común.

## Protección a meras ideas

Hace referencia a personas que dan ideas a corporaciones con la expectativa de ser recompensados.
  - Restricciones:
    1. Debe de ser original.
    2. Debe de ser concreta.
    3. La recompensa es obligatoria si se apropió de forma indebida.

## Derechos morales: sistemas continentales de PI

Protección de derechos personales de los creadores, distintos a los derechos económicos,
en el sentido de que protegen cualquier tipo de injuria al creador.

# Justificación y críticas

Existen tres formas de justificación:
  1. Personalistas: PI es una extensión de la personalidad.
  2. Utilitaristas: PI para el progreso social.
  3. Lockeana: PI como mérito y trabajo.

  => Tienen fortalezas y debilidades.

## Justificaciones personalistas de la PI

Los teóricos personalistas mantienen que el creador posee derechos morales por su talento,
sentimientos, carácter y experiencias.
  - Ej. Hegel.

=> Autopertenencia:
  - PI como autoactualización de sí.
  
  => La propiedad es importante para ello (Hegel).
    1. Manipulación de objetos para la liberación.
    2. En ocasiones nuestra personalidad se funde con el objeto.

### Dificultades

Existen al menos cuatro problemas:
  1. No es claro que nos pertenecen nuestros sentimientos, carácter y experiencias.
    * Una cuestión es poseer o ser, otra la de pertenecer.
  2. No es claro que nuestra personalidad pueda expandirse hacia los objetos.
    * Podría ser un abandono de la personalidad.
    * La mala representación de una obra puede cambiar nuestra percepción de un autor,
    pero no su personalidad.
  3. Lo único que se justificaría *per se* es el derecho de permitir o prohibir la alteración
  y no un DPI.
  4. Hay muchas creaciones que carecen de rasgos evidentes de personalidad.

### Réplica

Existe cierta índole intuitiva sobre las teorías personalistas de la PI.
  - Los derechos morales permiten crear leyes que protegen al creador ante un perjuicio.

Las teorías personalistas apelan a otras consideraciones morales.
  - Hegel incluye un sistema de incentivos para promover las ciencias y beneficiar a la sociedad.

## Justificaciones utilitaristas de la PI

El sistema norteamericano de DPI se basa en:
  * Sistema de incentivos.
  * Sistema utilitarista.

  => Para la creación es necesario una garantía delimitada de derechos de PI
  para incentivar la creación.
    - Crea incentivos para el progreso social.

### Dificultades compartidas por ambos

Necesidad de evidencia empírica.
  - Muy difícil de obtener => debatible.

### Dificultades

1. Existen modelos de estimular la PI sin necesariamente conceder restricciones.
  - Subsidio estatal para propiedad pública.
  - Recompensas con base en impuestos o regalías sin monopolización.
2. Los secretos comerciales no son socialmente benéficos a largo plazo.
3. Los costos y beneficios del derecho de autor, patentes y secretos comerciales son
difíciles de determinar.

### Réplica

Los utilitaristas pueden asentir con ciertas críticas y aún así continuar con su postura,
bajo el argumento de que un sistema así es mejor que nada.
  - La mayoría de las críticas se enfocan en un problema de implementación.
  - Los utilitaristas hacen patente los costos necesarios para cambiar el sistema de DPI.

## Justificaciones lockeanas de la PI

Los individuos tienen el derecho de controlar los frutos de su trabajo.
  - Mezcla del trabajo con el objeto.

=> Argumento formal de Locke
  * Los individuos son dueños de su cuerpo y su trabajo.
  * Cuando se trabaja sobre un objeto que no les pertenece, se mezclan hasta no poder ser separados.
  * Porlo tanto, se generan derechos de control

    => Expansión a partir de bienes comunales.

### Objeciones a Locke

1. La idea de mezclar el trabajo con el objeto es incoherente.
  - Las acciones no se mezclan con objetos.
2. Las labores secundarias son tan importantes como las primarias.
3. La mezcla también puede considerarse como pérdida de trabajo.
4. La mezcla no acarrea autoría, sino derechos limitados.
5. Si los objetos empleados son sociales, los reclamos individuales no quedan justificados.

### Réplica

Las dificultades no pasan desapercibidas.
  - Posibilidad de reconstrucción del argumento.
    * Ofrecer derechos limitados a la sociedad.
      - Evita secretos comerciales.
      - Evita la pérdida de talentos.
      - Evita la fuga de talentos.

# Críticas generales a la PI

No solo hay críticas para cada justificación.
  - Críticas hacia el derecho de control de la PI.

## Información no es propiedad

La información no puede adueñarse.
  - No es algo tangible que pueda poseerse.

  => El término de PI es al menos poco significativo, sino que incoherente.

=> vs:
  1. No es claro que la posesión requiera de algo físico.
    - Puede ser el poder de excluir a otros de cierta información.
  2. Permanece posible que se le puedan conceder derechos exclusivos a los creadores.
    - Que no sea propiedad no quiere decir que no se pueda proteger de otra manera.

## Información no rivaliza

Los trabajos intelectuales no se agotan, por lo que carece de sentido protegerlos como propiedad.

  => Argumento formal:
    - Si un trabajo no rivaliza (consumible sin agotamiento),
    el máximo acceso ha de permitirse.
    - Los trabajos de derechos de autor, patentes y secretos comerciales no rivalizan.
    - Por lo tanto, caso contrario de la PI.

=> vs:
  1. No hay nexo completo entre no ser rival y garantía del máximo acceso.
    - P. ej., información de seguridad nacional, personal o pensamientos privados.
  2. No queda claro qué tanto la reproducción no autorizada no perjudica al creador.
    - Incluso en el caso donde no existió una compra.
  3. La no-rivalidad no es justificación para el derecho de acceso.
    - No es una cuestión moralmente relevante,
    como es el caso de los reclamos de titularidad.

## Información quiere ser libre

La información tiene consideraciones morales porque es una forma de vida que quiere ser sí misma.
  - Anhelo de libertad y gratuidad.

=> vs:
  1. Las entidades abstractas no tienen intereses.
  2. El deseo solo se da en seres concientes,
  en el resto se habla de pura expectativa.
  3. No se sigue que del interés exista un deseo de ser libre.
  4. Es contraintuitivo el buscar un argumento de cómo la libertad beneficia a la información.

## Libre discurso en contra de la PI

La PI restringe:
  1. Los métodos para adquirir ideas (secretos comerciales).
  2. El uso de las ideas (patentes).
  3. La expresión de las ideas (derechos de autor).

=> vs:
  1. La restricción es limitada y para fomentar la creación y diseminación.
    - A la larga se vuelve algo común.
  2. El libre discurso ilimitado puede inducir al libertinaje.
  3. Se protegen expresiones concretas, no ideas.
    - Una nueva expresión a lo máximo puede ser plagio y no poder obtener derechos,
    pero puede expresarse.

## Naturaleza social de la información

No debe de haber derechos perpetuos o exclusivos de la PI porque parten de un conocimiento común.
  - Una construcción de todos.

=> vs:
  1. Es cuestionable la posibilidad de que algo pueda pertenecer o ser merecido por la «sociedad».
  2. Si bien es fuerte en contra de la PI, es débil en otros ámbitos sociales de evaluación moral.
    - No incentiva otras mejoras sociales más que esta.
  3. Pago por su acceso aunque sea indirectamente.

## El costo de publicar información digital

El costo se debe a los costos de comercialización.

=> vs:
  - En los contenidos digitales sus costos son nulos o muy bajos.
    * Posibilidad de disminuir paulatinamente el precio hasta llegar a cero.
  
  => vs:
    1. La cuota, aunque sea alta en un principio, se supondría como justa.
    2. Supone comerciantes y compradores racionales.
    3. En ciertos casos la producción de un contenido digital es muy alto.
